
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

    private Map<String, String> translationTableSymbolToSequence = new HashMap<String, String>(); // forward
    private Map<String, String> translationTableSequenceToSymbol = new HashMap<String, String>(); // backward
    Trie parsedTree = null; // Pointer to my lovely tree
    private int bitCount = 0;
   // instance variables here!

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {

       // Well, we are able to satisfy this requiement ;);
       if (original == null) {
           throw new RuntimeException ("We cannot work with a NULL tree!");
       }
   }

    // ========== Getters / Setters ============

    /**
     * method to set class bitCount variable after encoding
     * @param value
     */
   public void setBitCount(int value) {
       this.bitCount = value;
   }

    /**
     * method to read class bitCount variable after encoding
     * @return number of bits
     */
    public int bitLength()
    {
        return this.bitCount;
    }

    /**
     * method to add a found key/value pair symbol=sequence to the hashmap
     * @param symbol
     * @param sequence
     */
    public void addToMap1(String symbol, String sequence)
    {
        this.translationTableSymbolToSequence.put(symbol, sequence);
    }

    /**
     * method to visualize symbol/sequence hashmap (for debugging)
     */
    public void showMap1()
    {
        System.out.println(this.translationTableSymbolToSequence);
    }

    /**
     * method to find a sequence associated to key, if any
     * @param symbol
     * @return sequence
     */
    public String getSequence(String symbol)
    {
        return this.translationTableSymbolToSequence.get(symbol);
    }

    /**
     * method to add a sequence=symbol pair to the reverse hashmap
     * @param sequence
     * @param symbol
     */
    public void addToMap2(String sequence, String symbol)
    {
        this.translationTableSequenceToSymbol.put(sequence, symbol);
    }

    /**
     * method to visualize sequence/symbol hashmap
     */
    public void showMap2()
    {
        System.out.println(this.translationTableSequenceToSymbol);
    }

    /**
     * Method to find a symbol associated to key sequence, if any
     * @param sequence
     * @return symbol
     */
    public String getSymbol (String sequence)
    {
        return this.translationTableSequenceToSymbol.get(sequence);
    }

    // Here start functions

    // Inspiration: https://rosettacode.org/wiki/Huffman_coding#Java

    /**
     * a greedy algorithm method to fill the frequency tree
     * @param tree a tree to fill
     * @param prefix the source to analyze
     */
    public void traverseMyTree(Trie tree, StringBuffer prefix) {
        assert tree != null;
         if (tree instanceof Leaf) {
             Leaf leaf = (Leaf)tree;
            String key = String.valueOf(leaf.value);
            // print out character, frequency, and code for this leaf (which is just the prefix)
            System.out.println(key + "\t" + leaf.frequency + "\t" + prefix + "\t\t" + prefix.length());
            // Two tables. For each stage of the conversion
            this.addToMap1(key, prefix.toString());
            this.addToMap2(prefix.toString(), key);

        } else if (tree instanceof Node) {
             // recursion
            Node node = (Node)tree;

            // traverse left
            prefix.append('0');
            traverseMyTree(node.left, prefix);
            prefix.deleteCharAt(prefix.length()-1);

            // traverse right
            prefix.append('1');
            traverseMyTree(node.right, prefix);
            prefix.deleteCharAt(prefix.length()-1);
        }
        // DEBUG this.showMap1();
        // DEBUG this.showMap2();
    }


    // inspiration and wisdom: https://en.wikipedia.org/wiki/Prefix_code
   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
       // https://rosettacode.org/wiki/Huffman_coding#Java

       System.out.println("============ Now encoding: ==============================");
       int s = origData.length;
       char input [] = new char[s];

       for (int i = 0; i < s; i++) {
            input[i] = (char) origData[i];
       }
        System.out.println(input);
       int[] tally = new int[256];
       for (int i = 0; i < s; i++) {
           System.out.print(input[i] + " ");
            tally[(input[i])]++;
       }
       // ======================STATUS: Frequency table FILLED
       System.out.println();
       System.out.println();
            Trie peak = null;
           PriorityQueue<Trie> trees = new PriorityQueue<Trie>();
           // initially, we have a forest of leaves
           // one for each non-empty character
           for (int i = 0; i < tally.length; i++) {
               if (tally[i] > 0)
                   trees.offer(new Leaf(tally[i], (char) i));
           }
           assert trees.size() > 0;
           // loop and gather the parts into a compound tree
           while (trees.size() > 1) {
               // two trees with least frequency
               Trie a = trees.poll();
               Trie b = trees.poll();
               // put these into a new node and re-insert into queue
               trees.offer(new Node(a, b));
           }
           // size = 1 and we'll take away a united tree with a single peak (node or leaf)
            peak = trees.poll();
            this.parsedTree = peak; // make it globally available for decode()
       // ====================== STATUS: The miraculous greedy algorithm has succeeded

       // We pre-manipulate with the SB here due to recursive method later
       StringBuffer toBeFilled = new StringBuffer();
       if (peak instanceof Leaf) {
           toBeFilled.append('0'); // needed for a greedy test ;)
       }
       System.out.println("DEBUG: Filling out the frequency table:\n");
       System.out.println("Ch Freq PCode\tPrefix_length");
       traverseMyTree(peak, toBeFilled);
       System.out.println();
        // ====================== STATUS: the tree has been filled with the content
       // (and, my lovely hashmaps, too)

        // The actual encoding
       StringBuffer prefix = new StringBuffer();
       StringBuffer debug = new StringBuffer();
       System.out.print("DEBUG: Sequencing it by particles:");
       for (int i = 0; i < s; i++) {
           int ascii = Byte.toUnsignedInt(origData[i]);
           char tmp = (char) ascii;
           String sym = String.valueOf(tmp);
           prefix.append(this.getSequence(sym));
           debug.append(this.getSequence(sym) + " ");
       }
       System.out.println();
       System.out.println(debug); // a variant with spaces for better debug
       System.out.println();
       System.out.println("DEBUG Final encoded string: " + prefix);

       this.setBitCount(prefix.length());
       System.out.println("DEBUG: Useful Bit Length = " + bitLength());
       System.out.println();
       int modulo = (bitLength() % 8);
       int paddingSize = (8 - modulo) % 8;
       System.out.println("DEBUG: Modulo=" + modulo + ", we should pad " + paddingSize + " bits.");
       // ====================== STATUS: anything essential is done,
       // Now format conversion:
       return convertBytesToBits(prefix);
   }

    /**
     * an auxiliary method to backtranslate from a specific notation,
     * where bits are expressed by bytes 1 or 0
     * @param byteStream an octet using special notation
     * @return a correct 8-bit array
     */
    public byte[] convertBytesToBits (StringBuffer byteStream) {
       int s = byteStream.length();
       int modulo = (s % 8);
       int extraByte = 0;
       if (modulo != 0) extraByte = 1;
       int totalBytes = (s/8) + extraByte;
       int paddingCount = (totalBytes * 8) - s;
       String padString = "";
       for (int i=0; i < paddingCount; i++) {
           padString = padString + "0";
       }
       System.out.println("\ttotal octets in sequence: " + totalBytes + ", the last one padded by " + paddingCount + " bits.");
        System.out.print("DEBUG: useful sequence + padding: ");
        System.out.print(byteStream);
        System.out.print(" + " + padString +  "\n\n");

        // Operating in 8 byte chunks - octets
        byte[] result = new byte[(totalBytes)];
        for (int i = 0; i < totalBytes; i++) {
            String tmp = "";
            // Special case first - the last octet that needs padding
            if (i == (totalBytes - 1)) {
                tmp = byteStream.substring(0,(8 - paddingCount));
                tmp = tmp + padString;
            } else {
                // normal next octet
                tmp = byteStream.substring(0,8);
            }
            result[i] = octetToByte(tmp);
            byteStream.delete(0,8); // a permissive one
        }
       return result;
    }

    /**
     * an auxiliary method to convert 8 byte pseudo octets to real bytes
     * @param eightBytes
     * @return
     */
    public byte octetToByte (String eightBytes) {
        byte result = 0x00;
        if (eightBytes.length() != 8)
            System.out.println
                    ("ERROR: Function prepareOneByte: I need eight bytes");
        StringBuffer src = new StringBuffer();
        src.append(eightBytes);
        int bufferSize = src.length();
        int token = 0x00;
        int one = 0x01;
        // Miraculosly and losslessly ;) converting bytes to bits
        for (int k = 0; k < bufferSize; k++) {
            char tmp = src.charAt(0);
            if (tmp == '1') {
                token = token | one; // Bit arithmetics
            } else {
            }
            if (k != 7) {
                // The last round does not need shifting.
                token = token << 1; //
            }
            src.deleteCharAt(0);
        }
        token = token & 0xff;
        result = (byte)token;
        return result;
    }


   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {

       System.out.println("============ Now decoding: ==============================");
       // convert datatype to a contiguous one
       int size = encodedData.length;
       byte [] result = new byte [size];
       StringBuffer dst = new StringBuffer();
       StringBuffer output = new StringBuffer();
       int tester = 0x80;
       System.out.println("DEBUG: encodedData(): bytesize: " + size + ", bitsize: " + (size*8));
       for (int i = 0; i < size; i++) {
           int token = encodedData[i];
           for (int j = 7; j >= 0; j--) {
               // System.out.print( "i:" + i + " j:" + j + " | ");
               if ((token & tester) > 0) {
                   dst.append("1");
               } else {
                   dst.append("0");
               }
               token = token << 1; // next bit
           }
       }

        System.out.println("DEBUG:   bitsec + garbage: " + dst.toString() + "\n");

       // Working with the tree
       int totalCount = 0;

        System.out.print("DEBUG: the backtranslation table we use: ");
        this.showMap2();

       int bitCount = 0;
       boolean gameover = false;

       while ((dst.length() != 0) && !gameover) {
           int byteCount = 0;
           boolean solved = false;
           String suspected = "";
           String probe = "";
           while (!solved) {
               String decodedAs = "";
               suspected = suspected + dst.charAt(0); // permissive
               dst.deleteCharAt(0);
               bitCount++;
               probe = getSymbol(suspected);
               if (null != probe) {
                   decodedAs = probe;
                   output.append(probe);
                   solved = true;
                   System.out.println("Decoded: " + decodedAs + " as " + suspected +
                           ", at position " + ((size*8) - dst.length()));
                   if (bitCount == this.bitCount) gameover = true; // only garbage left
                   byteCount++;
               }
           }
       }
       String outputString = output.toString();
       System.out.println("\nDEBUG: printout of the final array:");
       System.out.println(outputString);
       System.out.println();
       result = outputString.getBytes();
      return result;
   }



   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      // String tekst = "A";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Encoded. Length of the encoded data in bits: " + lngth);
   }



    /**
     * This is the reTRIEval tree Data Structure to use for Huffman traversal
     * some reasonable inspiration from:
     *   - http://enos.itcollege.ee/~ylari/I231/Huffman.java
     *   - https://rosettacode.org/wiki/Huffman_coding#Java
     */
    abstract class Trie implements Comparable<Trie> {
        public final int frequency; // the frequency of this tree
        public Trie(int freq) { frequency = freq; }

        // compares on the frequency
        public int compareTo(Trie tree) {
            return frequency - tree.frequency;
        }
    }

    class Leaf extends Trie {
        public final char value; // the character this leaf represents

        public Leaf(int freq, char val) {
            super(freq);
            value = val;
        }
    }

    class Node extends Trie {
        public final Trie left, right; // subtrees

        public Node(Trie l, Trie r) {
            super(l.frequency + r.frequency);
            left = l;
            right = r;
        }
    }

}

